package com.mule.employee.flow.test;

import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.http.internal.ParameterMap;
import org.mule.munit.common.mocking.MessageProcessorMocker;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.munit.runner.functional.FunctionalMunitSuite;


/**
 * @author George Titan
 * @version 1.0
 * @since 18/03/2017
 * This is a test class for employee service module flow. I uses Mule Munit framework
 * to unit test smaller sections of the flow or sub flows and finally the entire flow as
 * end to end for integration test objective. The test cases are not complete and more cases can be written 
 */
public class EmployeeFlowTest extends FunctionalMunitSuite {
	
	@Rule
    public TestName testName = new TestName();
	
	private static final Logger LOGGER = LogManager.getLogger(EmployeeFlowTest.class.getName());
	
	private MuleMessage muleMessage, returnedMuleMessage;
	
	@Before
	public void setUp() {
		switch (testName.getMethodName()) {
		case "testInboundMessageWithGetMethodIntegerValueValidationFail" : {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("empno","dddddd");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodIntegerValueValidationSuccess" : {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("empno","490000");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodEnumValueValidationFail": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("gender","KK");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodEnumValueValidationSuccess": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("gender","F");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodParameterNameValidationFail": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("emp","490000");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodParameterNameValidationSuccess": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("empno","490000");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodParameterMissingValidationFail": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithGetMethodParameterMissingValidationSuccess": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new HashMap(){{put("gender","M");}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithPostMethodJsonToObject": {
			muleMessage = muleMessageWithPayload("{\"firstname\" : \"Walter\",\"lastname\" : \"Hill\",\"birth_date\" : \"1976-12-10\",\"gender\":\"M\",\"hire_date\" : \"2017-12-10\",\"salary\":\"69000\",\"title\":\"Senior Staff\",\"department\":\"Finance\"}");
			muleMessage.setProperty("http.method", "POST", PropertyScope.INBOUND);
			returnedMuleMessage =  muleMessageWithPayload(new HashMap(){{put("firstname","Jerry");put("lastname","Baloney");put("birth_date","1976-12-10");put("gender","M");put("hire_date","2017-12-10");put("salary","69000");put("title","Senior Staff");put("department","Finance");}});
			returnedMuleMessage.setProperty("http.method", "POST", PropertyScope.INBOUND);
			returnedMuleMessage.setProperty("http.request.path", "/v1/employee", PropertyScope.INBOUND);
			returnedMuleMessage.setProperty("http.query.params", new ParameterMap(){{}}, PropertyScope.INBOUND);
			break;
		}
		case "testInboundMessageWithPostMethod": {
			muleMessage = muleMessageWithPayload("{\"firstname\" : \"Walter\",\"lastname\" : \"Hill\",\"birth_date\" : \"1976-12-10\",\"gender\":\"M\",\"hire_date\" : \"2017-12-10\",\"salary\":\"69000\",\"title\":\"Senior Staff\",\"department\":\"Finance\"}");
			muleMessage.setProperty("http.method", "POST", PropertyScope.INBOUND);
			muleMessage.setProperty("http.request.path", "/v1/employee", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new ParameterMap(){{}}, PropertyScope.INBOUND);
			returnedMuleMessage =  muleMessageWithPayload(new String());
			break;
		}
		case "testInboundMessageWithDeleteMethod": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "DELETE", PropertyScope.INBOUND);
			muleMessage.setProperty("http.request.path", "/v1/employee", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new ParameterMap(){{put("empno","500001");}}, PropertyScope.INBOUND);
			returnedMuleMessage =  muleMessageWithPayload(new String());
			break;
		}
		case "testInboundMessageWithGetMethod": {
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "GET", PropertyScope.INBOUND);
			muleMessage.setProperty("http.request.path", "/v1/employee/current", PropertyScope.INBOUND);
			muleMessage.setProperty("http.query.params", new ParameterMap(){{put("empno","500000");}}, PropertyScope.INBOUND);
			returnedMuleMessage =  muleMessageWithPayload(null);
			break;
		}
		}
	}
	
	@After
	public void tearDown() {
		muleMessage = null;
		returnedMuleMessage=null;
	}
	
	@Test
	 public void testInboundMessageWithGetMethodIntegerValueValidationFail() {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodIntegerValueValidationFail()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		 try {
			MessageProcessorMocker mocker =
				      whenMessageProcessor("is-number").ofNamespace("validation"); 
			 mocker.thenReturn(returnedMessage);
			 MuleEvent testNumericValue = testEvent(muleMessage);	
			 MuleEvent resultMuleEvent =
			 runFlow("employee-service-module_v1.0.0Flow", testNumericValue); 
		 } catch (Exception exp) 
		 {
			 assertTrue(exp.getMessage().startsWith("Invalid input parameter"));
		 }
		 LOGGER.info("testing testInboundMessageWithGetMethodIntegerValueValidationFail() complete!");
	 }
	
	 @Test
	 public void testInboundMessageWithGetMethodIntegerValueValidationSuccess() throws Exception{
		
		 LOGGER.info("testing testInboundMessageWithGetMethodIntegerValueValidationSuccess()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		 MessageProcessorMocker mocker =
				      whenMessageProcessor("is-number").ofNamespace("validation"); 
		 mocker.thenReturn(returnedMessage);
		 MuleEvent testNumericValue = testEvent(muleMessage);	
		 MuleEvent resultMuleEvent =
	     runFlow("employee-service-module_v1.0.0Flow", testNumericValue);
		 assertSame(null,resultMuleEvent.getMessage().getExceptionPayload());	 
		 LOGGER.info("testing testInboundMessageWithGetMethodIntegerValueValidationSuccess() complete!");
	 }
	
	 @Test
	 public void testInboundMessageWithGetMethodEnumValueValidationFail() {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodEnumValueValidationFail()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		 try {
			MessageProcessorMocker mocker =
				      whenMessageProcessor("matches-regex").ofNamespace("validation"); 
			 mocker.thenReturn(returnedMessage);
			 MuleEvent testEnumValue = testEvent(muleMessage);		 
			 runFlow("employee-service-module_v1.0.0Flow", testEnumValue);
		 } catch (Exception exp) 
		 {
			 assertTrue(exp.getMessage().startsWith("Invalid value for gender"));
		 }
		 LOGGER.info("testing testInboundMessageWithGetMethodEnumValueValidationFail() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithGetMethodEnumValueValidationSuccess() throws Exception {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodEnumValueValidationSuccess()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		
		 MessageProcessorMocker mocker =
				whenMessageProcessor("matches-regex").ofNamespace("validation"); 
		 mocker.thenReturn(returnedMessage);
		 MuleEvent testEnumValue = testEvent(muleMessage);
		 MuleEvent resultMuleEvent =
		 runFlow("employee-service-module_v1.0.0Flow", testEnumValue);
		 assertSame(null,resultMuleEvent.getMessage().getExceptionPayload());
		 LOGGER.info("testing testInboundMessageWithGetMethodEnumValueValidationSuccess() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithGetMethodParameterNameValidationFail() {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterNameValidationFail()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		 try {
			MessageProcessorMocker mocker =
				      whenMessageProcessor("matches-regex").ofNamespace("validation"); 
			 mocker.thenReturn(returnedMessage);
			 MuleEvent testParamName = testEvent(muleMessage);		 
			 runFlow("employee-service-module_v1.0.0Flow", testParamName);
		 } catch (Exception exp) 
		 {
			 assertTrue(exp.getMessage().startsWith("Invalid parameter name"));
		 }
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterNameValidationFail() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithGetMethodParameterNameValidationSuccess() throws Exception {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterNameValidationSuccess()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		
		 MessageProcessorMocker mocker =
				whenMessageProcessor("matches-regex").ofNamespace("validation"); 
		 mocker.thenReturn(returnedMessage);
		 MuleEvent testParamName = testEvent(muleMessage);
		 MuleEvent resultMuleEvent =
		 runFlow("employee-service-module_v1.0.0Flow", testParamName);
		 assertSame(null,resultMuleEvent.getMessage().getExceptionPayload());
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterNameValidationSuccess() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithGetMethodParameterMissingValidationFail() {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterMissingValidationFail()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		 try {
			MessageProcessorMocker mocker =
				      whenMessageProcessor("matches-regex").ofNamespace("validation"); 
			 mocker.thenReturn(returnedMessage);
			 MuleEvent testMissingParam = testEvent(muleMessage);		 
			 runFlow("employee-service-module_v1.0.0Flow", testMissingParam);
		 } catch (Exception exp) 
		 {
			 assertTrue(exp.getMessage().startsWith("Parameter 'empno' or 'gender' is missing"));
		 }
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterMissingValidationFail() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithGetMethodParameterMissingValidationSuccess() throws Exception {
		
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterMissingValidationSuccess()....");
		 MuleMessage returnedMessage = muleMessageWithPayload(null); 
		
		 MessageProcessorMocker mocker =
				whenMessageProcessor("matches-regex").ofNamespace("validation"); 
		 mocker.thenReturn(returnedMessage);
		 MuleEvent testMissingParam = testEvent(muleMessage);
		 MuleEvent resultMuleEvent =
		 runFlow("employee-service-module_v1.0.0Flow", testMissingParam);
		 assertSame(null,resultMuleEvent.getMessage().getExceptionPayload());
		 LOGGER.info("testing testInboundMessageWithGetMethodParameterMissingValidationSuccess() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithPostMethodJsonToObject() throws Exception {

		LOGGER.info("testing testInboundMessageWithPostMethodJsonToObject()....");
		MessageProcessorMocker mocker =
				whenMessageProcessor("json-to-object-transformer").ofNamespace("json"); 
		mocker.thenReturn(returnedMuleMessage);
		MuleEvent testPostValue = testEvent(muleMessage);
		runFlow("employee-service-module_v1.0.0Flow", testPostValue);
		assertTrue("{firstname=Jerry, gender=M, birth_date=1976-12-10, hire_date=2017-12-10, salary=69000, title=Senior Staff, department=Finance, lastname=Baloney}".equals(returnedMuleMessage.getPayload().toString()));
		LOGGER.info("testing testInboundMessageWithPostMethodJsonToObject() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithPostMethod() throws Exception {

		LOGGER.info("testing testInboundMessageWithPostMethod()....");
		MessageProcessorMocker mocker = whenMessageProcessor("listener").ofNamespace("http"); 
		mocker.thenReturn(returnedMuleMessage);
		MuleEvent testPost = testEvent(muleMessage);
		MuleEvent resultMuleEvent =runFlow("employee-service-module_v1.0.0Flow", testPost);
		assertSame(returnedMuleMessage.getPayload().getClass(), resultMuleEvent.getMessage().getPayload().getClass());
		assertTrue(resultMuleEvent.getMessage().getPayload().toString().startsWith("Employee") && 
				       resultMuleEvent.getMessage().getPayload().toString().endsWith("was inserted!"));
		LOGGER.info("testing testInboundMessageWithPostMethod() complete!");
	 }
	 
	 @Test
	 public void testInboundMessageWithGetMethod() throws Exception {

		LOGGER.info("testing testInboundMessageWithGetMethod()....");
		MessageProcessorMocker mocker =
				whenMessageProcessor("listener").ofNamespace("http"); 
		mocker.thenReturn(returnedMuleMessage);
		MuleEvent testGet = testEvent(muleMessage);
		MuleEvent resultMuleEvent = runFlow("employee-service-module_v1.0.0Flow", testGet);
		assertTrue(resultMuleEvent.getMessage().getPayload() != null);
		assertTrue(resultMuleEvent.getMessage().getPayload().toString().contains("first_name"));
		assertTrue(resultMuleEvent.getMessage().getPayload().toString().contains("last_name"));
		LOGGER.info("testing testInboundMessageWithGetMethod() complete!");
	 }

	 @Test
	 public void testInboundMessageWithDeleteMethod() throws Exception {

		LOGGER.info("testing testInboundMessageWithDeleteMethod()....");
		MessageProcessorMocker mocker =
				whenMessageProcessor("listener").ofNamespace("http"); 
		mocker.thenReturn(returnedMuleMessage);
		MuleEvent testDelete = testEvent(muleMessage);
		MuleEvent resultMuleEvent = runFlow("employee-service-module_v1.0.0Flow", testDelete);
		assertSame(returnedMuleMessage.getPayload().getClass(),resultMuleEvent.getMessage().getPayload().getClass());
		assertTrue(resultMuleEvent.getMessage().getPayload().toString().equals("Employee " + ((Map)muleMessage.getInboundProperty("http.query.params")).get("empno") + " was deleted!"));
		LOGGER.info("testing testInboundMessageWithDeleteMethod() complete!");
	 }
}
