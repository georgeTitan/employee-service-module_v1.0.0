package com.employee.data.validators.test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.transport.NullPayload;

import com.employee.data.validators.EmployeeDataValidator;



/**
 * @author George Titan
 * @version 1.0
 * @since 18/03/2017
 * This is a test class for EmployeeDataValidator class. This is only a java
 * validator white box test.   
 *
 */
public class EmployeeDataValidatorTest extends FunctionalMunitSuite {
	
	@Rule
    public TestName testName = new TestName();
	
	private MuleMessage muleMessage;
	private Map<String, String> payloadData;
	private EmployeeDataValidator employeeDataValidator;
	
	@Before
	public void setUp() {

		switch (testName.getMethodName()) {
		case "testFullMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "Conway");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "2015-02-01");
			payloadData.put("gender", "M");
			payloadData.put("title", "Senior developer");
			payloadData.put("salary", "75000");
			payloadData.put("department", "Research");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}
		case "testEmptyFieldMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "2015-02-01");
			payloadData.put("gender", "M");
			payloadData.put("title", "Senior developer");
			payloadData.put("salary", "75000");
			payloadData.put("department", "Research");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}
		case "testNullFieldMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "Conway");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "2015-02-01");
			payloadData.put("gender", "M");
			payloadData.put("title", null);
			payloadData.put("salary", "75000");
			payloadData.put("department", "Research");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}

		case "testEnumFieldMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "Conway");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "2015-02-01");
			payloadData.put("gender", "T");
			payloadData.put("title", "Senior developer");
			payloadData.put("salary", "75000");
			payloadData.put("department", "Research");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}

		case "testNumericFieldMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "Conway");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "2015-02-01");
			payloadData.put("gender", "M");
			payloadData.put("title", "Senior developer");
			payloadData.put("salary", "75000.67");
			payloadData.put("department", "Research");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}

		case "testDateFieldMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "Conway");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "23022014");
			payloadData.put("gender", "M");
			payloadData.put("title", "Senior developer");
			payloadData.put("salary", "75000");
			payloadData.put("department", "Research");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}
		case "testGetMethodMessageAccept": {
			payloadData = null;
			employeeDataValidator = new EmployeeDataValidator();
			muleMessage = muleMessageWithPayload(null);
			muleMessage.setProperty("http.method", "get", PropertyScope.INBOUND);
			break;
		}
		case "testMissingFieldMessageAccept": {
			payloadData = new HashMap<String, String>();
			employeeDataValidator = new EmployeeDataValidator();
			payloadData.put("firstname", "Steve");
			payloadData.put("lastname", "Conway");
			payloadData.put("birth_date", "1966-05-12");
			payloadData.put("hire_date", "23022014");
			payloadData.put("gender", "M");
			payloadData.put("title", "Senior developer");
			payloadData.put("salary", "75000");
			muleMessage = muleMessageWithPayload(payloadData);
			muleMessage.setProperty("http.method", "post", PropertyScope.INBOUND);
			break;
		}
		}
	}
	
	@After
	public void tearDown() {
		muleMessage = null;
		payloadData = null;
		employeeDataValidator = null;
	}
	

	@Test
	public void testFullMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		boolean validationResult = employeeDataValidator.accept(muleMessage);
		assertTrue(validationResult); // mule message is valid
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyFieldMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		assertTrue(((Map<String,Object>)muleMessage.getPayload()).
				   entrySet().stream().anyMatch(i -> i.getValue().toString().isEmpty()));
		employeeDataValidator.accept(muleMessage);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNullFieldMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		assertTrue(((Map<String,Object>)muleMessage.getPayload()).
				   entrySet().stream().anyMatch(i -> i.getValue() == null));
		employeeDataValidator.accept(muleMessage);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEnumFieldMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		assertTrue(((Map<String,Object>)muleMessage.getPayload()).
				   entrySet().stream().anyMatch(i -> "gender".equals(i.getKey()) && 
						   !"M".equals(i.getValue().toString()) && !"F".equals(i.getValue().toString())));
		employeeDataValidator.accept(muleMessage);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNumericFieldMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		assertTrue(((Map<String,Object>)muleMessage.getPayload()).
				   entrySet().stream().anyMatch(i -> "salary".equals(i.getKey()) && 
						   !i.getValue().toString().matches("\\d+"))); // value for salary is not an integer
		employeeDataValidator.accept(muleMessage);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testDateFieldMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		assertTrue(((Map<String,Object>)muleMessage.getPayload()).
				   entrySet().stream().anyMatch(i -> "hire_date".equals(i.getKey()) && 
						   !i.getValue().toString().matches("yyyy-MM-dd"))); // value for date is not good format
		employeeDataValidator.accept(muleMessage);
	}
	
	@Test
	public void testGetMethodMessageAccept() {
		assertNull(payloadData);
		assertSame(NullPayload.getInstance().getClass(), muleMessage.getPayload().getClass());
		assertTrue("get".equals(muleMessage.getInboundProperty("http.method"))); // http method is not post so no payload necessary to validate
		assertTrue(employeeDataValidator.accept(muleMessage));
	}
	
	@Test(expected=NullPointerException.class)
	public void testMissingFieldMessageAccept() {
		assertNotNull(payloadData);
		assertNotNull(muleMessage.getPayload());
		assertTrue(((Map<String,Object>)muleMessage.getPayload()).
				   entrySet().stream().noneMatch(i -> "department".equals(i.getKey())));
		employeeDataValidator.accept(muleMessage);
	}

}
