package com.employee.data.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;

import org.mule.api.MuleMessage;
import org.mule.api.routing.filter.Filter;

public class EmployeeDataValidator implements Filter {
	
	enum RequiredPostFields {
		firstname,lastname,birth_date,gender,hire_date,salary,title,department
	}
	enum ValidDepartments {
		Marketing("Marketing"),Finance("Finance"),HumanResources("Human Resources"),Production("Production"),Development("Development"),
		QualityManagement("Quality Management"),Sales("Sales"),Research("Research"),CustomerService("Customer Service");
		
		private String department;
		ValidDepartments(String department) {
			this.department = department;
		}
		String getDepartment() {
			return this.department;
		}
	}
	
	@Override
	public boolean accept(MuleMessage message) {
		
		if (!"post".equalsIgnoreCase(message.getInboundProperty("http.method"))) {
			return true; // if the request is read or delete then there is no payload
		}
		
        @SuppressWarnings("unchecked")
		Map<String, Object> payloadMap = (Map<String, Object>) message.getPayload();
        
        // validate if all required fields are present
        for (RequiredPostFields required: RequiredPostFields.values()) {
        	if (!payloadMap.containsKey(required.toString())) { 
    			throw new NullPointerException(required + " is missing");	
            }
        }
    
        // validate date formatted fields
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try{
        	formatter.parse(payloadMap.get("birth_date").toString());
        } catch (ParseException parseExp) {
        	throw new IllegalArgumentException("birth_date format is invalid. Correct format is yyyy-MM-dd. Example: 2015-05-14");
        }
        try{
        	formatter.parse(payloadMap.get("hire_date").toString());
        } catch (ParseException parseExp) {
        	throw new IllegalArgumentException("hire_date format is invalid. Correct format is yyyy-MM-dd. Example: 2015-05-14");
        }
        
        // validate salary field
        try {
        	int salary = Integer.parseInt(payloadMap.get("salary").toString());
        	if (salary < 0) {
        		throw new NumberFormatException();
        	}
        } catch(NumberFormatException numberFormatExp) {
        	throw new IllegalArgumentException("salary value is invalid. Provide a positive whole number");
        }
        
        // validate gender field
        if (!"F".equals(payloadMap.get("gender").toString()) && !"M".equals(payloadMap.get("gender").toString())) {
        	throw new IllegalArgumentException("gender value is invalid. Correct value is M or F");
        }
        
        // validate department field
        if(Arrays.asList(ValidDepartments.values()).stream().map(i -> i.toString()).noneMatch(j -> j.equals(payloadMap.get("department").toString()))) {
    	   throw new IllegalArgumentException("department value is invalid. Correct value is one of: Marketing - Finance - Human Resources - Production - Development - Quality Management - Sales - Research - Customer Service");
        }
        
        // finally validate the values for firstname, lastname and title not to be null or empty
        if(payloadMap.entrySet().stream().anyMatch(i -> ("firstname".equals(i.getKey()) && (i.getValue() == null || i.getValue().toString().isEmpty()))) ||
        	payloadMap.entrySet().stream().anyMatch(i -> ("lastname".equals(i.getKey()) && (i.getValue() == null || i.getValue().toString().isEmpty()))) ||
        		payloadMap.entrySet().stream().anyMatch(i -> ("title".equals(i.getKey()) && (i.getValue() == null || i.getValue().toString().isEmpty())))) {
        	throw new IllegalArgumentException("The values for firstname, lastname and title cannot be null or empty.");		 
        }
        
		return true;
	}

}
